import passlib.hash
def lmhash(arg):
    return passlib.hash.lmhash.hash(str(arg)).upper()

class FilterModule(object):
    def filters(self):
        return {'lmhash': lmhash}

