from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

def zone_from_revdns(s):
    fields = s.split('.')
    # remove trailing 0s
    while fields[0] == '0':
        fields.pop(0)
    # remove . from the end
    if fields[-1] == '':
        fields.pop()
    return '.'.join(fields)

class FilterModule(object):
    def filters(self):
        return {
            'zone': zone_from_revdns
            }

