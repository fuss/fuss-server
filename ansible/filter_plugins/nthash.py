import passlib.hash
def nthash(arg):
    return passlib.hash.nthash.hash(str(arg)).upper()

class FilterModule(object):
    def filters(self):
        return {'nthash': nthash}

