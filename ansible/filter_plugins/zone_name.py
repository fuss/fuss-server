# this filter get zone name from a network address and its netmask
# netmask has to be passed as argument to the filter
#
def zone_name(s,n):
    addr = s.split('.')
    net = n.split('.')
    name = [ "in-addr.arpa" ]
    while net.pop(0) == "255":
        name.append(addr.pop(0))
    name.reverse()
    return '.'.join(name)

class FilterModule(object):
    def filters(self):
        return {
            'zone_name': zone_name
            }

