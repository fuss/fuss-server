
The keys in this directory are allowed to connect to this server to add a
fuss-client to it.

If you want to add more keys or change the existing ones, add both the
public and private keys to this directory and run fuss-server upgrade.

Deleting client-rsa and running fuss-server upgrade will create and enable a
new key with that name.
