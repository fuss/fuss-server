# Copyright (C) 2017 Simone Piccardi <piccardi@truelite.it>,
#                    Elena Grandi <elena@truelite.it>,
#                    Progetto Fuss <info@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA

---
- name: Check if we had successfully configured kerberos
  stat:
      path: /etc/krb5kdc/stash
  register: st
- name: Remove kerberos packages, if installed but not configured
  apt:
      state: absent
      purge: yes
      name: krb5-config,krb5-admin-server
  when: not st.stat.exists

- name: Preseeding kerberos realm
  debconf:
      name: krb5-config
      question: krb5-config/default_realm
      value: '{{ domain | upper}}'
      vtype: string
- name: Preseeding kerberos newrealm
  debconf:
      name: krb5-admin-server
      question: krb5-admin-server/newrealm
      value: ''
      vtype: string
- name: Install kerberos
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - krb5-admin-server
- name: Create kerberos realm
  shell: 'echo "{{ pass }}\n{{ pass }}" | krb5_newrealm'
  args:
      creates: /etc/krb5kdc/stash

- name: Configure kerberos realm
  blockinfile:
      dest: /etc/krb5.conf
      block: |
          {{ domain | upper}} = {
              kdc = {{ fqdn }}:88
              admin_server = {{ fqdn | lower }}:88
              default_domain = {{ domain | lower }}
          }
      insertafter: "\\[realms\\]"
      marker: "# {mark} ANSIBLE MANAGED REALM BLOCK"
  notify: restart kerberos

- name: test for kdc.conf modifications
  ini_file:
    path: /etc/krb5kdc/kdc.conf
    section: kdcdefaults
    option: kdc_tcp_listen
    value: '""'
  notify: restart krb5kdc

- name: Create kerberos admin principal
  shell: 'echo "addprinc root/admin\n{{ pass }}\n{{ pass }}" | kadmin.local'
  register: res
  changed_when: "not 'Principal or policy already exists' in res.stderr"

- name: Give full access to admin
  lineinfile:
      dest: /etc/krb5kdc/kadm5.acl
      line: 'root/admin@{{ domain | upper}} *'
      backup: yes
  notify: restart kerberos
  
- name: Create script to add principals for the clients
  template:
      dest: /usr/local/sbin/add_client_principal
      src: add_client_principal
      mode: 0755
- name: Create directory for client keys
  local_action: file
  args:
      dest: /etc/fuss-server/client_keys
      state: directory
      mode: 0700
- name: Add a readme to the client keys directory
  copy:
      src: README-client_keys.txt
      dest: /etc/fuss-server/client_keys/README.txt
- name: Create ssh key for fuss-client
  local_action: shell ssh-keygen -P '' -f /etc/fuss-server/client_keys/client-rsa
  args:
      creates: '/etc/fuss-server/client_keys/client-rsa'
- name: Add the client key to authorized keys
  authorized_key:
      user: root
      state: present
      key: "{{ lookup('file', item)}}"
      key_options: 'command="if [[ \"$SSH_ORIGINAL_COMMAND\" =~ ^/usr/lib/openssh/sftp-server ]] || [[ \"$SSH_ORIGINAL_COMMAND\" =~ ^add_client_principal ]] || [[ \"$SSH_ORIGINAL_COMMAND\" =~ rm[[:space:]]/root/ ]]; then $SSH_ORIGINAL_COMMAND; else echo \"Access Denied $SSH_ORIGINAL_COMMAND\"; fi"'
  with_fileglob:
      - '/etc/fuss-server/client_keys/*.pub'
- name: Configure user mapping in /etc/idmapd.conf
  template:
      dest: /etc/idmapd.conf
      src: idmapd.conf
      backup: yes
  notify:
      - restart nfs-idmapd
