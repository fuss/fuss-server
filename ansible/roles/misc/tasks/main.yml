# Copyright (C) 2016-2025 Progetto Fuss <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>,
#          Elena Grandi <elena@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA

---
- name: Install ntpsec server
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - ntpsec

# TODO: add wifi interfaces
- name: Configure time server
  lineinfile:
      backup: yes
      dest: /etc/ntpsec/ntp.conf
      line: "restrict {{fuss_eth[item]['ipv4']['network']}} mask {{fuss_eth[item]['ipv4']['netmask']}} nomodify notrap nopeer"
  notify:
      restart ntpsec
  with_items: "{{internal_ifaces}}"

- name: Configure time server
  lineinfile:
      backup: yes
      dest: /etc/ntpsec/ntp.conf
      line: "restrict {{fuss_eth[hotspot_iface]['ipv4']['network']}} mask {{fuss_eth[hotspot_iface]['ipv4']['netmask']}} nomodify notrap nopeer"
  notify:
      restart ntpsec
  when: hotspot_iface is defined and hotspot_iface
  
- name: Install OpenSSH server
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - openssh-server

- name: Create group sshaccess
  group:
      name: sshaccess
      state: present

- name: Configure OpenSSH client
  blockinfile:
      backup: yes
      dest: /etc/ssh/ssh_config.d/nohash.conf
      create: yes
      block: |
        # This file contains fuss-server added configuration for SSH client 
        # must be in the /etc/ssh/ssh_config.d/ directory
        # Configuration files are processed in alphabetical order
        
        # don't hash hostnames in known_hosts
        Host *
            HashKnownHosts no

- name: Configure OpenSSH server
  blockinfile:
      backup: yes
      dest: /etc/ssh/sshd_config.d/fuss-server.conf
      create: yes
      # TODO: check that these entries are not already set by the default
      # config (they are not in bookworm)
      block: |
        # This file contains fuss-server added configurations for SSH server
        # must be in the /etc/ssh/sshd_config.d/ directory
        # Configuration files are processed in alphabetical order
        
        # added for fuss-server 6.0
        AllowGroups root sshaccess
        # added after fuss-server 10.0
        PermitRootLogin yes
  notify:
      # This should be harmless because sshd keeps sessions while restarting
      restart sshd

- name: Setup SSHd to only listen in IPv4 if IPv6 is disabled
  blockinfile:
    dest: /etc/ssh/sshd_config.d/force_ipv4.conf
    backup: yes
    create: yes
    block: |
      # This file contains added configurations for the fuss-server
      # created when IPv6 is disabled
      AddressFamily inet
  when: disable_ipv6 is defined and disable_ipv6
  notify:
      # This should be harmless because sshd keeps sessions while restarting
      restart sshd

# It is important that /etc/skel/.profile does not try to load the old and no
# longer existing "$HOME/.fuss_profile", so we keep this here for some time. It
# can be removed later, possibly when migrating to bullseye
- name: source our customization from .profile in skel
  lineinfile:
      dest: /etc/skel/.profile
      line: '. "$HOME/.fuss_profile"'
      state: absent

- name: Install borgbackup
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - borgbackup

- name: Install cups and printer drivers
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - cups,foomatic-db-compressed-ppds

- name: Configure a cups ServerAlias for the name "proxy"
  lineinfile:
    path: /etc/cups/cupsd.conf
    insertafter: "MaxLogSize 0"
    regexp: "^ServerAlias"
    line: "ServerAlias proxy"
  notify: restart cups

- name: Install other dependencies required by the AP management scripts
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - sshpass,expect

- name: Create standard ldap groups
  command: "smbldap-groupadd -a {{ item }}"
  with_items: '{{ standard_ldap_groups }}'
  register: res
  changed_when: res.rc == 0
  failed_when: res.rc != 0 and 'exists' not in res.stderr

- name: Add a service script to apply network permissions
  copy:
      backup: yes
      src: propagate-net-perm
      dest: /etc/init.d/propagate-net-perm
      mode: 0755

- name: make systemd aware of the new script
  command: systemctl daemon-reload
  changed_when: False

- name: Add home cleanup script to cron
  cron:
    cron_file: home-cleanup
    name: home cleanup
    user: root
    job: /usr/share/fuss-server/scripts/home-cleanup
    hour: "{{ home_cleanup_time_hour }}"
    minute: "{{ home_cleanup_time_minute }}"

- name: Add home cleanup weekly script to cron
  cron:
    cron_file: home-cleanup-weekly
    name: home cleanup weekly
    user: root
    job: /usr/share/fuss-server/scripts/home-cleanup-weekly
    hour: "{{ home_cleanup_time_hour }}"
    minute: "{{ home_cleanup_time_minute }}"
    weekday: "{{ home_cleanup_weekly_time_weekday }}"

- name: Add var cleanup script to cron
  cron:
    cron_file: var-cleanup
    name: var cleanup
    user: root
    job: /usr/share/fuss-server/scripts/cleanvar.sh
    hour: 22
    minute: 15

- name: Add keytab cleanup script to cron
  cron:
    cron_file: keytab-cleanup
    name: PATH
    value: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    env: yes
    user: root
    job: /usr/share/fuss-server/scripts/keytabclean.sh
    hour: 22
    minute: 30

- name: Disable network-manager if required
  service:
    name: network-manager
    enabled: no
    state: stopped
  failed_when: false

- name: Install etherwake to turn clients on remotely
  include_tasks: "{{ includes }}/install-package-apt.yml"
  with_items:
      - etherwake
