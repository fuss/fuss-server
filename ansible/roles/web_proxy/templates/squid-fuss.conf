### This file contains all configuration added for FUSS
### they were previuosly inserted inside squid.conf
### now are included from /etc/squid/conf.d/

# LDAP authentication
auth_param basic program /usr/lib/squid/basic_ldap_auth -b "ou=Users,{{ basedn }}" -v3 -f "(uid=%s)" -h localhost
auth_param basic children 5
auth_param basic realm {{ domain }} Proxy Server
auth_param basic credentialsttl 30 minutes
auth_param basic utf8 on

# unix group auth 
#external_acl_type ldap_group %LOGIN /usr/lib/squid/squid_ldap_group -b "ou=Groups,{{ basedn }}" -B "ou=Users,{{ basedn }}" -f "(&(memberUid=%u)(cn=%g))" -h localhost
external_acl_type unix_group children-startup=3 %LOGIN /usr/lib/squid/ext_unix_group_acl -p

# added acl and no-cache directives
acl QUERY urlpath_regex cgi-bin \?
no_cache deny QUERY
#acl fuss.bz.it url_regex ^http://archive.fuss.bz.it/
#no_cache deny fuss.bz.it
acl localserver url_regex ^http://{{ host }}
no_cache deny localserver

follow_x_forwarded_for allow localhost

# TODO: verify why this rule was enabled and if it is still needed
http_access allow to_localhost

# include wifi captive portal acls, this is rewritten on updates, do not put
# any changes on it
include /etc/squid/{{cp_squid_acl_file}}

# BEGIN: OS check/block from useragent
{% if proxy_win_exclude %}
# Block access from Windows for any user, group, time, etc.
acl nowindows browser Windows
http_access deny nowindows
deny_info ERR_NO_WINDOWS nowindows
{% endif %}
# END: OS check/block from useragent

# include locally added list of repositories for unauthenticated access
include /etc/squid/squid-added-repo.conf
# allow unauthenticated access for repositories
http_access allow repositories
http_access allow repositories2
# allow unauthenticated access for firefox addons and components
acl firefox url_regex ^services.addons.mozilla.org
acl firefox url_regex ^versioncheck.addons.mozilla.org
acl firefox url_regex ^addons.mozilla.org
acl firefox url_regex ^it.add-ons.mozilla.com
acl firefox url_regex ^detectportal.firefox.com
acl firefox url_regex ^shavar.services.mozilla.com
acl firefox url_regex ^clientservices.googleapis.com
http_access allow firefox
# allow access to server itself
acl itself dst {{server_ip}}
http_access allow itself
acl itself2 url_regex ^http://proxy/
http_access allow itself2
# allow access to the fuss clients network (LAN)
acl lannet dst {{localnet}}
no_cache deny lannet
http_access allow lannet

# Now needed also for wifi (ex captive portal) access
acl password proxy_auth REQUIRED

#acl internet external ldap_group internet # not used
acl internet external unix_group internet
{% if squid_require_internet_group %}
http_access allow password internet
{% else %}
http_access allow password
{% endif %}
# Decomment following lines commenting previous one to enable alternate 
#acl our_networks src {{localnet}}
#acl orario time  M T W H F 7:00-22:00 A 7:00-14:00
#http_access allow password internet orario our_networks

{% if not e2guardian_install %}
# add also e2guardian port when non installing it
http_port 8080
{% endif %}

# Configure cache setup parameters
cache_mem {{ squid_memory }} MB
maximum_object_size 200 MB
cache_dir aufs /var/spool/squid  {{ squid_disk_space }} 16 256

# Logging configurations
logformat squidforwarded %ts.%03tu %6tr %{X-Forwarded-For}>h %Ss/%03Hs %<st %rm %ru %un %Sh/%<A %mt
access_log daemon:/var/log/squid/access.log squidforwarded
access_log daemon:/var/log/squid/useragent.log useragent
error_directory /usr/share/squid/errors/Italian

# still in use?
max_filedescriptors {{squid_filedescriptors}}
