#! /bin/bash

# Usage:
# ./test.sh 
#
# Copyright (C) 2016 Simone Piccardi <piccardi@truelite.it>,
#                    Elena Grandi <elena@truelite.it>,
#                    Progetto Fuss <info@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA

# exit on error

set -e

# Log file (temporary)
LOGFILE=$(tempfile)
LOGFILECP=$(tempfile)

# to add tests just write a function that must give an espected
# result, compute the shasum of the espected result and put it in the
# CHECK array variable 


function test_ldap() {
    . /etc/smbldap-tools/smbldap.conf
    ldapsearch -x '(uid=admin)' \
	| egrep -v "$SID|dn: uid=admin|^#|sambaPwdLastSet:|shadowLastChange:" \
	| tee $LOGFILE \
	| shasum \
	| awk '{print $1}'
}

# testing functions
function test_samba () {
   pdbedit -L | egrep 'admin:|nobody:' | tee $LOGFILE | shasum |awk '{print $1}'
}

function test_nslcd () {
   getent passwd | egrep 'admin:|nobody:'| tee $LOGFILE |shasum |awk '{print $1}'
}

function test_web () {
    wget http://localhost/fuss-data-conf/id_rsa.pub -O - -q \
        | tee $LOGFILE \
	| shasum \
	| awk '{print $1}'
}

function test_webpwd () {
    wget http://localhost/webpasswd -O - -q \
	| tee $LOGFILE \
	| shasum \
	| awk '{print $1}'
}

function test_smbauth () {
  if smbldap-useradd -m -a provaprova > $LOGFILE; then
    smbldap-passwd provaprova <<EOF >> $LOGFILE
pippo
pippo
EOF
    echo pwd \
	| smbclient //localhost/provaprova -U provaprova%pippo 2>/dev/null \
	| tee -a $LOGFILE \
	| shasum \
	| awk '{print $1}'
    smbldap-userdel provaprova >> $LOGFILE
    rm -fR /home/provaprova
  else
    echo error >> $LOGFILE
  fi
}

function test_octofussd () {
    . /etc/smbldap-tools/smbldap_bind.conf
    OCTOFUSS_PASSWORD=$masterPw
    OCTOFUSS_USER=root
    export OCTOFUSS_PASSWORD
    export OCTOFUSS_USER
    echo ls /users/users \
	| octofussctl http://localhost:13400/conf \
	| egrep 'admin|nobody' \
	| tee $LOGFILE \
	| shasum \
	| awk '{print $1}'
}



# expected results
declare -A CHECK
CHECK["test_webpwd"]=ebe75b0a0778db4c2aabc5c30c218df6d35c1724
CHECK["test_samba"]=cfbcd34dbb9ad01817950846578d25ff9b63f83b
CHECK["test_nslcd"]=6e9e69a888fa7fb46dce610cff93abfafa8c848e
CHECK["test_ldap"]=7bb0793c7d1476582ddef8c3f8c94a3ab36afb87
CHECK["test_web"]=$(cat /root/.ssh/id_rsa.pub|shasum|awk '{print $1}')
CHECK["test_smbauth"]=a73612cd28351c8c7d004661c0fbf901d507e492
CHECK["test_octofussd"]=0fbc80307b68597807679b314c98a418730fdecf
# expected results for captive portal
declare -A CHECKCP

# testing loop
for i in ${!CHECK[@]}; do
    if [ "$($i)" = ${CHECK[$i]} ]; then
	echo check $i successfull
    else
	echo check $i failed
	echo registered output was:
	cat $LOGFILE
    fi
done

if [ -f /etc/fuss-server/fuss-captive-portal.conf ]; then
# testing loop
for i in ${!CHECKCP[@]}; do
    if [ "$($i)" = ${CHECKCP[$i]} ]; then
	echo check $i successfull
    else
	echo check $i failed
	echo registered output was:
	cat $LOGFILECP
    fi
done
    
fi

# remove log
rm -f $LOGFILE
rm -f $LOGFILECP
