===========
fuss-server
===========

-----------------------
configure a FUSS server
-----------------------

:Date: 2021-02-26
:Version: 10.0
:Manual section: 8

SYNOPSIS
--------

fuss-server [-h] {create,upgrade,configure,purge,test,cp} ...

DESCRIPTION
-----------

This manual page briefly documents the fuss-server command.

COMMONLY USED COMMANDS
----------------------

create
   Configure all service and create file needed to run as a FUSS server.
upgrade
   Upgrade the configuration of an existing FUSS server.
configure
   Create or recreate a configuration file for the FUSS server.

   -r, --reconfigure-all
      Reconfigure all options
   -b, --bootstrap
      Delete all current configuration and start with a new empty file
   -f CONFIGURATION_FILE, --configuration-file CONFIGURATION_FILE
      Use a different configuration file (for testing)

purge
   Purge all FUSS server specific file, to consent a clean use of create
test
   Test the configuration of a FUSS server.
cp
   Configure a captive portal 

FILES
-----

/etc/fuss-server/*

.. Build the manpage with rst2man fuss-server.8.rst > fuss-server.8
   See also
   http://docutils.sourceforge.net/sandbox/manpage-writer/rst2man.txt
