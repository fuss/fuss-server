#! /bin/bash

# pwdchange.sh, script per cambiare password ad un utente
# Copyright (C) 2006-2020 FUSS Project <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

usage () {
    echo "Usage: ./pwdchange.sh user newpass"
}

# check if run by root
if [ `id -u` != 0 ]; then 
    echo "You must be root to use this script"
fi

# you need two arguments
if [ "$#" -ne 2 ]; then
    usage
    exit 1
fi

# using variable names
USER=$1
PASS=$2

# where to log
LOG=/tmp/pwdchange-$(date +%F).log

FUSSCONF=/etc/fuss-server/fuss-server.yaml
OCTOURL=http://localhost:13400/conf
export OCTOFUSS_USER=root
export OCTOFUSS_PASSWORD=$(sed -nr 's/^pass: (.*)$/\1/p' $FUSSCONF)

echo -e "cd users/users/$USER\nset password $PASS"|octofussctl $OCTOURL >> $LOG
