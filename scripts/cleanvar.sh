#! /bin/bash

## Script to clean /var directory see #23010

## clean /var/log/octofuss
# Retention for octofussd and octofuss-client log, in days
OCTOLOG_RET=120
# removing old files
find /var/log/octofuss -mtime +$OCTOLOG_RET -delete

## clean local users mail
# mail retention for local users
MAIL_RET=120d
for i in $(ls /var/mail|grep -E '^[a-z]+$'); do
    [ -s /var/mail/$i ] || continue
    screen -d -m mutt -f /var/mail/$i -e "push D~d>$MAIL_RET<enter>qy<enter>" 
done
