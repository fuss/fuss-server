#!/bin/bash

# Simple script to turn off a list of machines
#
# Copyright (C) 2023 FUSS Project <info@fuss.bz.it>
# Authors: Claudio Cavalli <ccavalli@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# Usage:
#  ./cluster-poweroff.sh <file>   
# where file contains the list of machines to be turned off 
# In the file the first column contains the hostnames of the machines, 
# the second, their mac address


usage () {
    echo "Usage: ./cluster-poweroff.sh file"
}

CLUSTER=$1

if [ ! -e "$CLUSTER" ]; then
    usage
    exit 1
fi


for i in $(cat $CLUSTER | awk '{print $1}') ; do ssh $i poweroff ; done
