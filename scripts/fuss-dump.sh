#!/bin/bash

#
# fuss-dump.sh, script to dump a FUSS10 server data
# usage: ./fuss-dump.sh 
# save dump in fuss-server-dump-$(date +%F).tgz in the current dir
#
# Copyright (C) 2023 FUSS Project <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

# variables
DATAFILE=fuss-conf-dump.tar
LOG=fuss-dump.log

DUMPDIR=/var/backups/fuss-dump
# create backup dir
[ -d $DUMPDIR ] || mkdir -m 700 $DUMPDIR
cd $DUMPDIR

exec 2> $LOG

# Some messages
echo "Starting server data dump at $(date +%R)"
echo "files will be under $DUMPDIR"
echo

# Archive configuration/data files to be saved
echo "* archiving files"
tar -cf $DATAFILE /etc/fuss-server/client_keys
tar -uf $DATAFILE /etc/fuss-server/firewall-*
tar -uf $DATAFILE /etc/dhcp/dhcp-reservations
tar -uf $DATAFILE /etc/fuss-server/firewall-custom-rules
tar -uf $DATAFILE /etc/fuss-server/content-filter-allowed-sites
tar -uf $DATAFILE /etc/fuss-server/Credentials/id_rsa*
tar -uf $DATAFILE /var/lib/octofuss/octofuss.db
tar -uf $DATAFILE /etc/cups/printers.conf
tar -uf $DATAFILE /etc/cups/ppd/*
tar -uf $DATAFILE /etc/krb5kdc/stash
tar -uf $DATAFILE /etc/krb5.keytab
tar -uf $DATAFILE /etc/bind/named.added.conf.local
tar -uf $DATAFILE /etc/dhcp/dhcpd-added.conf
tar -uf $DATAFILE /root/.ssh/authorized_keys
tar -uf $DATAFILE /srv/clonezilla/computerList.txt
tar -uf $DATAFILE /etc/squid/squid-added-repo.conf 
# additional files (really needed?)
# tar -uf $DATAFILE /etc/e2guardian/lists/bannedsitelist 
# tar -uf $DATAFILE /etc/e2guardian/lists/bannedextensionlist

# optional files
for i in /etc/samba/shares.conf \
	 /etc/fuss-backup/fuss-backup.conf \
	 /etc/fuss-server/fuss-captive-portal.conf \
	 /etc/fuss-server/ap-config.sh \
	 /etc/clusters \
	 /root/.ssh/authorized_keys \
	 /root/.ssh/id_rsa \
	 /root/.ssh/id_rsa.pub \
	 /root/.ssh/known_hosts \
	 /etc/fuss-backup/fuss-backup.conf \
	 /etc/cron.d/fuss-backup \
	 /var/www/fuss-data-conf/ocsinventory.yml \
	 /var/www/fuss-data-conf/server.crt \
	 /root/bin \
	 /opt
do  
    if [ -e $i ]; then
	tar -uf $DATAFILE $i
    fi
done

##
## Saving other data outside files
##

# Dumps of service data
echo "* dumping LDAP and Kerberos data"
slapcat -l ldap-dump.ldif
kdb5_util dump > krb5.dump

# Quota settings for the homes

QUOTA=$(grep usrquota /etc/fstab | awk '{print $2}')
if [ x$QUOTA != x'' ]
then
    echo "* dumping user quota data"
    repquota -u $QUOTA > quota-users.txt
fi
QUOTA=$(grep grpquota /etc/fstab | awk '{print $2}')
if [ x$QUOTA != x'' ]
then
    echo "* dumping group quota data"
    repquota -n -g $QUOTA > quota-groups.txt
fi

# old fuss-server configs are copied for reference
echo "* copying old fuss-server configuration"
cp /etc/fuss-server/fuss-server.yaml fuss-server.yaml.old 
cp /etc/fuss-server/fuss-server-defaults.yaml fuss-server-defaults.yaml.old

cd - > /dev/null
# put all data in a single file
DUMPFILE="fuss-server-dump-$(date +%F).tgz"
tar -czf $DUMPFILE -C /var/backups/ fuss-dump

# END
echo
echo "Dump completed at $(date +%R)"
echo "Created dump archive file on $DUMPFILE"
