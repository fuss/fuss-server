#!/bin/bash

#
# fuss-restore.sh, script to restore from a FUSS8 server data
# usage: ./fuss-restore.sh fuss-server-dump-$(date +%F).tgz 
#
# Copyright (C) 2023 FUSS Project <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

# local directory where the dump tar extracts its data
BASE=fuss-dump

function usage () {
    echo "Script to restore configuration data of a fuss server"
    echo "usage: ./fuss-restore.sh fuss-server-dump-$(date +%F).tgz"
    echo ""
    echo "   The data file fuss-server-dump-$(date +%F).tgz should be"
    echo "   created in the source server with fuss-dump.sh"
    exit 1
}

function restorequota () {
    QUOTA=$(grep usrquota /etc/fstab | awk '{print $2}')
    while read a b; do
	if [ "$b" = "0 0 0 0" ]; then
	    echo user $a has no quota
	else
	    echo setting quota for user $a
	    setquota -u $a $b $QUOTA
	fi
    done <<<$(cat $1|grep -v '^$'|tail -n+6| awk '{print $1,$4,$5,$7,$8}')
    QUOTA=$(grep grpquota /etc/fstab | awk '{print $2}')
    if [ -n "$QUOTA" ]; then
    while read a b; do
	if [ "$b" = "0 0 0 0" ]; then
	    echo group $(getent group ${a:1}|cut -d: -f1) has no quota
	else
	    echo setting quota for group $(getent group ${a:1}|cut -d: -f1)
	    setquota -g ${a:1} $b $QUOTA
	fi
    done <<<$(cat $2|grep -v '^$'|tail -n+6| awk '{print $1,$4,$5,$7,$8}')
    fi
}

## Syntax checks
if [ "$#" != 1 ]; then
    usage
fi
# check parameter
if ! [ -f $1 ]; then
    echo "dump file $1 do not exist or is not a file"
    usage
fi

# extract data
if tar -xzf $1; then
    echo "Dump data decompressed"
else
    echo "error on decompressing dump $1"
    usage
fi

# stop services
SERVICES="slapd nslcd krb5-admin-server krb5-kdc smbd nmbd octofussd cups
	  squid e2guardian isc-dhcp-server bind9"
echo "Stopping services"
for i in $SERVICES
do
    echo "  * Stopping $i"
    systemctl stop $i
done

# LDAP restore
echo "Restoring LDAP"
rm -rf /var/lib/ldap/*
slapadd < $BASE/ldap-dump.ldif
chown openldap:openldap /var/lib/ldap/*
# kerberos restore
echo Restoring Kerberos
kdb5_util load $BASE/krb5.dump
# restore files
echo Restoring files
tar -xvf  $BASE/fuss-conf-dump.tar -C /
# restore root SSH keys
echo Restoring root SSH keys
cp -f /etc/fuss-server/Credentials/id_rsa* /root/.ssh 

## NOTE: Remove this workaround on next iteration
# move dhcp-reservation to new location
mv -f /etc/fuss-server/dhcp-reservations /etc/dhcp/

# restart services
echo "Restarting services"
for i in $SERVICES
do
    echo " * starting $i"
    systemctl start $i
done


# restore quota if they are available
echo Restoring quotas
if ! [ -f $BASE/quota-users.txt ]; then
    echo "Missing users quota dump, cannot restore"
elif ! [ -f $BASE/quota-groups.txt ]; then
    echo "Missing groups quota dump, cannot restore"    
elif ! grep usrquota /etc/fstab > /dev/null; then
    echo "Missing user quota on destination, cannot restore"
elif ! grep grpquota /etc/fstab > /dev/null; then
    echo "Missing group quota on destination, cannot restore"
else
    restorequota $BASE/quota-users.txt  $BASE/quota-groups.txt
fi

echo "Restore script completed"

##
## aggiustamenti finali
##

# ripropaga al DNS le reservation
#/usr/share/fuss-server/scripts/dnsreserv.py
