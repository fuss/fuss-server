#! /bin/bash
#
# Simple script to change the password of a user group.
# In the first part (commented) you can assign a different password to each user in a csv list.
# In the second part you can choose a common password to assign to a group of ldap users.
# In case uncomment the first part of the script and comment on the second part
#
# Copyright (C) 2023 FUSS Project <info@fuss.bz.it>
# Authors: Claudio Cavalli <ccavalli@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


### First part of the script ###

#usage () {
#    echo "Usage: ./group-passwd-change.sh file"
#}

#FILE=$1

#if [ ! -e "$FILE" ]; then
#    usage
#    exit
#fi

# while IFS=, read -r USER PASSWORD
# do
#     /usr/share/fuss-server/scripts/chguserpwd.sh $USER $PASSWORD
# done < $FILE

### End of the first part ###

# ---------------------------------------------------------------------- #

### Second part of the script ###

echo "What is the group of users to whom you want to change the password?"
read GRUPPO
echo "Which password do you want to assign?"
read PASSWORD
for i in $(members $GRUPPO)
do
    echo $i
    /usr/share/fuss-server/scripts/chguserpwd.sh $i $PASSWORD
done

### End of the second part ###
