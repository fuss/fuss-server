#!/bin/bash

# Scripts for deleting specific files or emptying cache folders in users' homes
# Copyright (C) 2005-2023 FUSS Project <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#
# list of file/directory to clean
#
CLEAN_LIST="
.config/xfce4/xfconf/xfce-perchannel-xml/displays.xml
.cache/chromium/
.cache/google-chrome/
.cache/mozilla/firefox/
.config/chromium/Singleton*
.config/google-chrome/Singleton*
"

#
# do not touch files for following users;
# give a space separated list
#
EXCLUDED_USERS="admin nobody"

for i in $(getent.ldap passwd|tr -d " "); do
    USR=$(echo $i|cut -d: -f1)
    if ! echo $EXCLUDED_USERS | egrep "\b$USR\b" > /dev/null; then
	HOM=$(echo $i|cut -d: -f6)
	for j in $CLEAN_LIST; do
	    # echo cleaning $HOM/$j # uncomment for debug
	    rm -fR $HOM/$j*
	done
	fi
done
