#!/bin/bash

# Script to poweroff all machines in the LAN ; eventually whitelist the server                      
# Copyright (C) 2005-2023 FUSS Project <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

LAN=$(grep localnet: /etc/fuss-server/fuss-server.yaml | awk '{print $2}')

if ! [ $LAN ]; then
    echo cannot find LAN address, stopping
    exit 1
fi

LISTA=$(nmap -sn $LAN | grep 'Nmap scan report for' | awk '{print $5}')

SSH_OPTIONS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o BatchMode=yes"

declare -A WHITELIST
#WHITELIST["fuss-ser.fuss.blz"]=yes

for i in $LISTA; do
   if ! [ ${WHITELIST["$i"]} ]; then
       ssh $SSH_OPTIONS $i poweroff
   else
       echo $i is whitelisted
   fi
done
