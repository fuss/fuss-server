#!/bin/bash

# Simple script that turns off all machines in the dhcp range that are turned on.
# Typically it is used in conjunction with cron.d to perform shutdown at a certain time of all machines left on .
# 0 21 * * * root /usr/share/fuss-server/scripts/poweroff-machines.sh

# Copyright (C) 2023 FUSS Project <info@fuss.bz.it>
# Author: Claudio Cavalli <ccavalli@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

for i in `/root/scripts/leases.py -o | awk '{print $8}'|sort`; do echo $1; ssh -q -o ConnectTimeout=3 $i poweroff ; done

# In case you want to limit the shutdown to only some machines,
# you can introduce a "grep" command, as in the following example:
# for i in `/root/scripts/leases-test.py -o | grep 'dm\|ldv' | awk '{print $8}'|sort`; do echo $1; ssh -q -o ConnectTimeout=3 $i poweroff ; done

