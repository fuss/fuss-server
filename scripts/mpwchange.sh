#!/bin/sh
#
# mpwchange.sh, script per cambiare la master password 
# Copyright (C) 2006-2019 FUSS Project <info@fuss.bz.it>
# Author: Simone Piccardi <piccardi@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

usage () {
    echo "Usage: ./mpwchange.sh oldpass newpass"
    echo "       if you don't remember oldpass just look inside the file:"
    echo "       /etc/fuss-server/fuss-server.yaml"
    echo "       for the pass: variable definition "
    echo "       (before executing the script)"
}


##
## Defining used files, directories and program
##
CONF_FILE=/etc/fuss-server/fuss-server.yaml
CA_DIR=/etc/fuss-server/Credentials

if [ "$#" -ne 2 ]; then
    usage
    exit 1
fi

if [ `id -u` != 0 ]; then 
    echo "You must be root to use this script"
fi

# Configuration variables
if [ -f "$CONF_FILE" ]; then
    MASTER_PASS=$(sed -rn 's/^(.*)pass: (.*)$/\2/p' $CONF_FILE)
    DOMAIN=$(sed -rn 's/^(.*)domain: (.*)$/\2/p' $CONF_FILE)
    
else
    echo "Something wrong, missing configuration file $CONF_FILE"
    exit 1
fi


if [ "$MASTER_PASS" != "$1" ]; then
    echo "Wrong password!"
    exit 1
fi

# Setting other configuration variables
BASE=`echo $DOMAIN | 
       awk -F"." '{OFS=""; ORS=","; for (i=1; i <= NF; i++) print "dc=",$i}' | 
       sed -re 's/,$//g'`
USER="cn=admin,$BASE"

# Configuration file correctin
echo "mpwchange: changing password in configuration file $CONF_FILE"
cp $CONF_FILE ${CONF_FILE}.new
sed -r "s/pass: $1/pass: $2/g" ${CONF_FILE}.new  \
    > $CONF_FILE 
rm ${CONF_FILE}.new

# Admin di LDAP (no more in DB)
echo "mpwchange: changing LDAP admin password"
LDAP_PASS=$(slappasswd -h {CRYPT} -c '$6$%.16s' -s $2)
LDAP_CONF=/etc/ldap/slapd.conf
cp $LDAP_CONF ${LDAP_CONF}.new
sed -r "s|^(rootpw.*)([{].*)|\1$LDAP_PASS|g" ${LDAP_CONF}.new  \
    > $LDAP_CONF
rm ${LDAP_CONF}.new
service slapd restart

# ldapvi access
echo "mpwchange: changing ldapvi admin access password"
cp /root/.ldapvirc /root/.ldapvirc.new
sed -r "s/password: $1/password: $2/g" /root/.ldapvirc.new > /root/.ldapvirc
rm /root/.ldapvirc.new

# per accesso di Samba a LDAP
echo "mpwchange: changing Samba/LDAP password"
smbpasswd -w "$2"

# accesso di smbldap a LDAP
echo "mpwchange: changing smbldap-tools password"
SMBLDAPF=/etc/smbldap-tools/smbldap_bind.conf
cp $SMBLDAPF ${SMBLDAPF}.new
cat ${SMBLDAPF}.new \
    | sed -r "s/$1/$2/g" \
    > $SMBLDAPF
rm -f ${SMBLDAPF}.new

# administrator del dominio
echo "mpwchange: changing domain admin password"
/usr/sbin/smbldap-passwd admin <<EOF
$2
$2
EOF

# configurazione octofuss/octonet
echo "mpwchange: changing octofuss/LDAP password"
cp /etc/octofuss/octofuss.conf /etc/octofuss/octofuss.conf.new
sed -r "s/bindpw\s*=\s*$1/bindpw = $2/g" /etc/octofuss/octofuss.conf.new \
    > /etc/octofuss/octofuss.conf
rm /etc/octofuss/octofuss.conf.new
service octofussd restart

# configurazione accesso octonet/octofussctl
echo "mpwchange: changing octofuss connection password"
octofussd --reset-root-password $2

# Kerberos admin
echo "mpwchange: changing kerberos root/admin"
echo "cpw root/admin\n$2\n$2" | kadmin.local
