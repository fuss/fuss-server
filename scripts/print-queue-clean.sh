#! /bin/bash

# Simple script to permanently remove PCs from a print queue
# Copyright (C) 2024 FUSS Project <info@fuss.bz.it>
# Author: Claudio Cavalli <ccavalli@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# Function to prompt user for confirmation
get_confirmation() {
    while true; do
        read -p "$1 (yes/no): " choice
        case $choice in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer 'yes' or 'no'.";;
        esac
    done
}

# Function to get the list of net printers
getPrinters(){
    for i in $(octofussctl $OCTOURL ls printers/byqueue/)
    do
        list="$i $list"
    done

    for j in $list
    do
        nrList="$j - $nrList"
    done

    echo $nrList
}

FUSSCONF=/etc/fuss-server/fuss-server.yaml
OCTOURL=http://localhost:13400/conf
OCTOFUSS_PASSWORD=$(sed -nr 's/^pass: (.*)$/\1/p' $FUSSCONF)
export OCTOFUSS_PASSWORD="$OCTOFUSS_PASSWORD"
export OCTOFUSS_USER=root

printer=$(dialog --title "FUSS FUCC" --output-fd 1 --menu "Choose a printer" 15 50 4 $(getPrinters))
clear
if [ "$printer" != "" ]; then
    echo "If you proceed in the script, PCs  will be permanently deleted from the print queue"
    if get_confirmation "Do you want to perform this action?"; then
        if get_confirmation "Are you sure you want to proceed? This is your final chance."; then
            echo "Action confirmed, executing..."echo "Action confirmed, executing..."

            for c in $(octofussctl $OCTOURL ls /printers/byqueue/"$printer")
            do
                octofussctl $OCTOURL delete /printers/byqueue/"$printer"/"$c"
            done
        else
            echo "Final confirmation declined."
        fi
    else
        echo "Action canceled."
    fi
fi

