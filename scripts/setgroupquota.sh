#!/bin/bash

# Simple script to add user quotas to a group of users                   
# Copyright (C) 2005-2023 FUSS Project <info@fuss.bz.it>
# Authors: Simone Piccardi <piccardi@truelite.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

usage () {
    echo "Usage: ./setgroupquota.sh group softblockquota hardblockquota [mountpoint]"
}

if ! [ $# -eq 3 -o $# -eq 4 ]; then
   usage
   exit 1
fi

if [ -z "$4" ]; then
   MOUNTPOINT=/home
else
   MOUNTPOINT=$4
fi

for i in $(members $1); do
   quotatool -b -q $2 -l $3 -u $i $MOUNTPOINT
done
