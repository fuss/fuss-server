#!/bin/bash

#
# ssh-fingerprints --  A script to print all SSH server fingerprint
#
# Copyright (C) 2018 FUSS Project <info@fuss.bz.it>
# Author: Simone Piccardi <piccardi@truelite.it> and Truelite Srl
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

ssh-keyscan $(hostname) 2>/dev/null  | ssh-keygen -l -f -
