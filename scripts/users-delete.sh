#! /bin/bash

# Simple script to permanently remove a list of users and their homes
# Copyright (C) 2023 FUSS Project <info@fuss.bz.it>
# Author: Claudio Cavalli <ccavalli@fuss.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# Usage:
#  ./users-delete.sh <file>   
# where file contains the list of users to be deleted

usage () {
    echo "Usage: ./users-delete.sh file"
}

LIST=$1

if [ ! -e "$LIST" ]; then
    usage
    exit 1
fi


# Function to prompt user for confirmation
get_confirmation() {
    while true; do
        read -p "$1 (yes/no): " choice
        case $choice in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer 'yes' or 'no'.";;
        esac
    done
}

echo "If you proceed in the script, the home of the users listed in user_list.txt "
echo "and then the users themselves will be PERMANENTLY deleted."
if get_confirmation "Do you want to perform this action?"; then
    if get_confirmation "Are you sure you want to proceed? This is your final chance."; then
        echo "Action confirmed, executing..."
    
        FUSSCONF=/etc/fuss-server/fuss-server.yaml
        OCTOURL=http://localhost:13400/conf
        export OCTOFUSS_PASSWORD=$(sed -nr 's/^pass: (.*)$/\1/p' $FUSSCONF)
        export OCTOFUSS_USER=root

        for u in `cat $LIST`
        do
            home=`smbldap-usershow $u |grep homeDirectory|awk '{print $2}'`
            rm -fr $home
            octofussctl $OCTOURL delete /users/users/$u
            echo $u     
        done
        else
            echo "Final confirmation declined."
    fi
else
    echo "Action canceled."
fi

